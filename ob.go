package main

import (
	"fmt"
	"strings"
	"sync"

	"gocv.io/x/gocv"

	"gitlab.com/okotech/okofrm"
	"okotek.ai/x/okocfg"
)

type matBox struct {
	Mat     gocv.Mat
	CapTime uint64
	UName   string
	UPass   string
}

func main() {

	intake := make(chan matBox)
	toSendoff := make(chan okofrm.Frame)
	cfg := okocfg.GetDatPuller()
	procAddrs := strings.Split(cfg("okobld")["listenPort"], ",")

	go okofrm.MatBoxListen(intake, procAddrs[0])
	go frameBuilder(intake, toSendoff)
	go dumpFrames(toSendoff)
	//go okonet.Sendoff(toSendoff)

	var stopper sync.WaitGroup
	stopper.Add(123456)
	stopper.Wait()
}

// Threadpool version
func _frameBuilder(input chan okofrm.MatBox, output chan okofrm.Frame) {

	interChan := make(chan okofrm.MatBox)

	//Spin up threadpools
	for count := 0; count < 10; count++ {
		go func(input chan okofrm.MatBox) {
			mtbx := <-input
			output <- mtbx.ConvertToFrame()
		}(interChan)
	}
}

func frameBuilder(input chan okofrm.MatBox, output chan okofrm.Frame) {
	for mb := range input {
		go func(mtbx okofrm.MatBox) {
			tmpFrm := mtbx.ConvertToFrame()
			output <- tmpFrm
		}(mb)
	}
}

func dumpFrames(input chan okofrm.Frame) {
	for inp := range input {
		fmt.Println(inp.Channels())
	}

}

//Dr Steven Edberg
